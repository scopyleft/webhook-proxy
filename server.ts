async function urlToB64(url: string): Promise<string> {
    const response = await fetch(url)
    const blob = await response.blob()
    const buff = await blob.arrayBuffer()
    return Buffer.from(buff).toString('base64')
}

const server = Bun.serve({
    port: process.env.PORT || 3000,
    async fetch(req) {
        try {
            const proxy = await req.json()
            proxy.body.actions[0].content = await urlToB64(proxy.body.actions[0].content)
            proxy.body.actions[0].encoding = "base64"

            console.debug(JSON.stringify(proxy))

            const response = await fetch(proxy.url, {
                method: proxy.method,
                headers: proxy.headers,
                body: JSON.stringify(proxy.body)
            })
            const result = await response.text()
            console.info(response.status, result)
            return new Response(JSON.stringify(result))
        } catch (e) {
            console.error(e)
            const text = await req.text()
            const msg = `Failed to parse proxy request: ${text}`
            console.error(msg)
            return new Response(msg)
        }
    },
})

console.log(`Listening on localhost: ${server.port}`)
