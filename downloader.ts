async function urlToB64(url: string): Promise<string> {
  const response = await fetch(url)
  const blob = await response.blob()
  const buff = await blob.arrayBuffer()
  return new Blob([buff])
  // return Buffer.from(buff).toString('base64')
}

const server = Bun.serve({
  port: process.env.PORT || 3000,
  async fetch(req) {
    const url = new URL(req.url)
    const fileURL = url.searchParams.get("url") || ""
    const fileName = url.searchParams.get("name") || "download.pdf"
    const buffer = await urlToB64(fileURL)
    const response = new Response(buffer)
    response.headers.set("Content-Type", "application/pdf")
    response.headers.set("Content-Disposition", `attachment;filename=${fileName}`)
    return response
  }
})

console.log(`Listening on port ${server.port}`)
