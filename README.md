# Webhook Proxy

Proxy a request to another URL.
Transform the content on the way.

## Requirements

[Bun](https://bun.sh/) is the only requirement to run this server.
No extra install required.

## Running the server

```shell
bun server.ts
```

## Deployment (AlwaysData)

Run `make deploy`

Note you should have an access to the server first! ;p
